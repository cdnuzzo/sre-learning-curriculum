# sre-learning-curriculum

Becoming a SysAdmin, Systems Engineer, or Site Reliability Engineer takes some technical know-how. Here I lay out a plan.

###  [The Missing Semester](https://missing.csail.mit.edu/)


### The Open Source CS Degree

https://github.com/ForrestKnight/open-source-cs

## Programming

### Python

[Automate the Boring Stuff](https://automatetheboringstuff.com/)

[Learn by Building a Blockchain](https://www.udemy.com/course/learn-python-by-building-a-blockchain-cryptocurrency/)

### C

[Linux Programming with C edX Certificate courses](https://www.edx.org/professional-certificate/dartmouth-imtx-c-programming-with-linux)
[Inside C Standard Lib](https://begriffs.com/posts/2019-01-19-inside-c-standard-lib.html)


## Linux Academy Resources 

[Linux Foundation Certified Engineer course](https://linuxacademy.com/cp/courses/lesson/course/2262/)


Nginx Deep Dive -  https://linuxacademy.com/cp/courses/lesson/course/1876

Vim - https://linuxacademy.com/cp/modules/view/id/85

Docker Deep Dive - https://linuxacademy.com/course/docker-deep-dive-part-1/

SQL Deep Dive - https://linuxacademy.com/cp/modules/view/id/407

PostgresSQL Administration Deep Dive - https://linuxacademy.com/cp/modules/view/id/477

Mastering Systemd - https://linuxacademy.com/cp/modules/view/id/171

Mastering Regular Expressions - https://linuxacademy.com/course/mastering-regular-expressions/

Linux Capacity Planning - https://linuxacademy.com/cp/modules/view/id/511

"DevOps Tools" - https://linuxacademy.com/cp/modules/view/id/217

Linux User Management Deep Dive - https://linuxacademy.com/cp/modules/view/id/443

Source Control with Git - https://linuxacademy.com/cp/modules/view/id/195

Intro to Python Development - https://linuxacademy.com/cp/modules/view/id/311

ECS Deep Dive- https://linuxacademy.com/cp/modules/view/id/261


Labs:

[Deploy Python Flask to AWS ECS](https://app.linuxacademy.com/hands-on-labs/7981daef-9996-418a-a1ec-29ba5e749558)

## AWS

[Networking in the Cloud Part 1 of 12 by Last Week in AWS](https://www.lastweekinaws.com/podcast/aws-morning-brief/networking-in-the-cloud-fundamentals-part-1/)


## Networking
VLANs, OSI, DNS, LACP

[History of the URL](https://blog.cloudflare.com/the-history-of-the-url/)


## Linux Kernel

[Linux Kernel Labs](https://linux-kernel-labs.github.io/refs/heads/master/index.html)

[Advanced Linux Kernel Understanding](https://www.linkedin.com/learning/advanced-linux-the-linux-kernel/)

[book on LeanPub](https://leanpub.com/linuxkernel)


## Security 

[Binary Exploitation and Memory Corruption Playlist](https://www.youtube.com/playlist?list=PLhixgUqwRTjxglIswKp9mpkfPNfHkzyeN)
[Crypto Algos Compared](https://www.cryptologie.net/article/497/eddsa-ed25519-ed25519-ietf-ed25519ph-ed25519ctx-hasheddsa-pureeddsa-wtf/)

## System Design

https://github.com/donnemartin/system-design-primer

## Hardware

BIOS, BMCs, UEFI, PXE boot, x86 



## Jobs, jobs, jobs, jobs Resources

WeWorkRemotely https://weworkremotely.com/categories/remote-devops-sysadmin-jobs

## Further Reading

[How To Become an SRE from Gremlin](https://www.gremlin.com/site-reliability-engineering/how-to-become-a-top-notch-sre/)
[Personal account of path to SRE](https://danrl.com/blog/2019/path-to-srm/)
[How To Get Into SRE by Alice](https://blog.alicegoldfuss.com/how-to-get-into-sre/)


## Build Your Own

[GitHub Build Your Own repo](https://github.com/danistefanovic/build-your-own-x)

## For Fun
Factorio game